# Docker Laravel

## Setup
```bash
cp env.example .env  # update to taste
docker compose up -d --build
```

Go to http://localhost:8000 (replace 8000 with $EXTERNAL_APP_PORT if you updated it) to preview your app.
