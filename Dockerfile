FROM php:8.3-apache

# Update dependencies
RUN <<END
apt-get update
apt-get install -y libzip-dev zip
docker-php-ext-install pdo_mysql zip
END

RUN a2enmod rewrite

# Ensures the app is served from /public by replacing it inside Apache config files
ENV APACHE_DOCUMENT_ROOT=/var/www/html/public
RUN <<END
sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf
END

# Copy the app inside. Useful if you want the container to run standalone
COPY . /var/www/html

WORKDIR /var/www/html

RUN <<END
curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
composer install --optimize-autoloader --no-dev
END

# Permissions for some Laravel folders
RUN chown -R www-data:www-data /var/www/html/storage /var/www/html/bootstrap/cache
